# splunkcli Readme

[Video Runthrough](https://ea99fb50f04662878542.s3.amazonaws.com/SyncViews.mp4)

## Current ToDo/Done Features
- [x] GetViews
- [x] GetView
- [x] ExportViews
- [x] CreateView
- [x] DeleteView
- [x] GetApps
- [x] DeleteApp
- [x] CreateUpdateApp
- [ ] GetAlerts
- [ ] ExportAlerts
- [X] SyncViews
- [ ] SyncAlerts


## What is this?
This is a program, written in the fabulous Go language that abstracts the Splunk REST API to facilitate doing things like:
* Dashboard creation / deletion
* Alert creation / deletion
* Menu adjustments

The purpose of this CLI is to help facilitate CI/CD processes. In other words, you put this CLI into a Runner/Agent to scan a Git repo with the Splunk XML/YAML files that represent dashboards/alerts, and it will provision your Splunk instance.

## Prerequisites
* Docker
* Docker-Compose
* Go (if running locally)

## Operations

### Building from Source
* Compiling / building: `make build`
* Running tests: `make showcoverage` or, `make tests`

### Setting up
* Set up configuration. This can take the form of:
  * Envvars
  * A YAML config file set at $HOME, called `.splunkcli.yaml`
    * The settings should be something similar to:
```
SPLUNK_URL: https://localhost:8089
SPLUNK_USER: youruser
SPLUNK_PASSWORD: yourpassword
SPLUNK_DIRECTORY: directory-to-use
```
  * The splunk URL is your base URL, the user/password is what the API will be called with via Basic Auth.
  * The Directory is used for exporting, or using for source

### Running
* Execute `./splunkcli` with the appropriate verbs and parameters.  

#### Running Splunk
* Get the docker image:
```
docker pull splunk/splunk:latest
```
* Start it up in docker:
```
docker run -d -p 8000:8000 -p 8089:8089 -e "SPLUNK_START_ARGS=--accept-license" -e "SPLUNK_PASSWORD=password" --name splunk splunk/splunk:latest
```
* Wait a minute or so, then hit http://localhost:8000 and sign in to verify things are working. Use `admin` and `password` as the user / password.
#### Shutting down Docker
* Shut down container: 
```
docker container kill $(docker ps -q)
```
* Kill the container:
```
docker container rm <containerid>
```

#### Options
* Get all apps: `./splunkcli apps getapps`. This lists all the configured apps for the Splunk instance.
* Get all views (dashboards): `./splunkcli views getviews <appname>`. This lists all the views (dashboards) for a given app.
* Export all views (dashboards): `./splunkcli views exportviews <appname>`. This will do same as above, but will write out to file, in the directory specified by the envvar SPLUNK_DIRECTORY.
* Create/Update View: `./splunkcli views createupdateview <appname> <filepath(xml)>`
* Delete View: `./splunkcli views deleteview <appname> <viewname>`
* Sync views: `./splunkcli views syncviews <appname>`

### Reference
Below are the equivalent curl commands to a Splunk Enterprise instance that were used to build this cli.
* to get apps:
```
curl -ku user:password  https://localhost:8089/services/apps/local
```
* To create an app:
```
curl -ku user:password -X POST https://localhost:8089/services/apps/local -d 'name=blahblah'     
```
* To delete an app:
```
curl -ku user:password -X DELETE https://localhost:8089/services/apps/local/blahblah
```
* To get views:
```
curl -ku user:password  https://localhost:8089/servicesNS/-/<appname>/data/ui/views
```
* To Get a particular view:
```
curl -ku user:password  https://localhost:8089/servicesNS/-/<appname>/data/ui/views/<viewname>
```
* To create a view:
```
curl -ku user:password -X POST https://localhost:8089/servicesNS/allen/<appname>/data/ui/views -d 'name=firstclassic' -d 'eai:data=<dashboard>
  <label>firstclassic</label>
  <description>firstclassic</description>
  <row>
    <panel>
      <table>
        <search>
          <query>| from inputlookup:"geo_attr_countries"</query>
          <earliest>-24h@h</earliest>
          <latest>now</latest>
        </search>
        <option name="drilldown">none</option>
      </table>
    </panel>
  </row>
</dashboard>'

```