/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/xml"
	"fmt"
	"github.com/spf13/cobra"
	"net/url"
)

type Apps struct {
	XMLName xml.Name `xml:"feed"`
	AppEntries   []AppEntry `xml:"entry"`
}
type AppEntry struct {
	XMLName xml.Name `xml:"entry"`
	Title string `xml:"title"`
}

// splunkCmd represents the apps command
var appsCmd = &cobra.Command{
	Use:   "apps",
	Short: "Splunk apps functionality",
	Long: `This interfaces with the Splunk REST API`,
	//Run: func(cmd *cobra.Command, args []string) {
	//},
}
var GetSplunkAppsCmd = &cobra.Command{
	Use:   "getapps",
	Short: "Gets list of Splunk Apps",
	Long: `This interfaces with the Splunk REST API, and gets list of apps.`,
	Run: func(cmd *cobra.Command, args []string) {
		GetSplunkApps()
	},
}
func GetSplunkApps() error {
	//follows the pattern: curl -ku user:password https://localhost:8089/services/apps/local/
	url := fmt.Sprintf("%s/services/apps/local", splunkURL)
	data, err := SplunkClient.HttpProcessor("GET", url, splunkUser, splunkPassword, nil)
	if err != nil {
		fmt.Println(err)
		return err
	}
	_, err = ParseSplunkApps(data)
	return err
}
func CreateSplunkApp(appname string) error {
	params := url.Values{}
	params.Add("name", appname)

	url := fmt.Sprintf("%s/services/apps/local", splunkURL)
	_, err := SplunkClient.HttpProcessor("POST", url, splunkUser, splunkPassword, []byte(params.Encode()))
	return err
}
func DeleteSplunkApp(appname string) error {
	params := url.Values{}
	params.Add("name", appname)
	url := fmt.Sprintf("%s/services/apps/local/%s", splunkURL, appname)
	_, err := SplunkClient.HttpProcessor("DELETE", url, splunkUser, splunkPassword, nil)
	return err
}
func ParseSplunkApps(appsXML []byte) (Apps, error) {
	var apps Apps
	err := xml.Unmarshal(appsXML, &apps)
	if err == nil {
		for idx, value := range apps.AppEntries {
			fmt.Printf("Splunk App #%d: %s\n", idx+1, value.Title)
		}
	} else {
		fmt.Printf("Error: %s", err.Error())
	}
	return apps, err
}

func init() {
	appsCmd.AddCommand(GetSplunkAppsCmd)
	rootCmd.AddCommand(appsCmd)
	SplunkClient = &splunkWebImpl{}
}
