/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"io/ioutil"
	"os"
	"path"
	"runtime"
	"testing"
	"github.com/spf13/viper"
)
type appsDataWebMock struct{}
func (me *appsDataWebMock) HttpProcessor(verb, url, username, password string, payload []byte) ([]byte, error) {
	//equivalent CURL:
	//curl -ku user:password https://localhost:8089/services/apps/local/
	appsXML, err := ioutil.ReadFile("fixtures/getapps.xml")
	return appsXML, err
}
func setup(){
	//Get this setup to current directory for future fixture loading like beasts.
	_, filename, _, _ := runtime.Caller(0)
	// The ".." may change depending on you folder structure
	dir := path.Join(path.Dir(filename), "..")
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}
}
func teardown(){
	//nothing here
}
func TestMain(m *testing.M){
	setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}
func Test_ParseSplunkApps(t *testing.T) {
	SplunkClient = &appsDataWebMock{}
	xmlFile, err := os.Open("../fixtures/getapps.xml")
	if err != nil {
		t.Errorf("Could not load fixture.")
	}
	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		t.Errorf("Fixture isn't read correctly.")
	}
	apps, err := ParseSplunkApps(byteValue)
	if err != nil {
		t.Errorf("Error reading apps.")
	}
	if len(apps.AppEntries) < 2 {
		t.Errorf("There's something wrong with the Apps return.")
	}
}
func Test_CRUDApps(t *testing.T) {

	SplunkClient = &splunkWebImpl{}
	viper.Set("SPLUNK_URL", os.Getenv("SPLUNK_URL"))
	viper.Set("SPLUNK_USER", os.Getenv("SPLUNK_USER"))
	viper.Set("SPLUNK_PASSWORD", os.Getenv("SPLUNK_PASSWORD"))
	collectFlags()
	err := CreateSplunkApp("testtest")
	if err != nil {
		t.Errorf("Error creating app: %s", err)
	}
	err = DeleteSplunkApp("testtest")
	if err != nil {
		t.Errorf("Error deleting app: %s", err)
	}
}
func Test_ParseSplunkViews(t *testing.T) {
	SplunkClient = &appsDataWebMock{}
	xmlFile, err := os.Open("../fixtures/getviews.xml")
	if err != nil {
		t.Errorf("Could not load fixture.")
	}
	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		t.Errorf("Fixture isn't read correctly.")
	}
	views, err := ParseSplunkViews(byteValue,"" , false)
	if err != nil {
		t.Errorf("Error reading views.")
	}
	if len(views.Entry) < 2 {
		t.Errorf("There's something wrong with the Views return.")
	}
}
func Test_CreateUpdateDeleteView(t *testing.T) {
	//this is designed to run in a container
	SplunkClient = &splunkWebImpl{}
	appname := "testerblah"
	err := CreateSplunkApp(appname)
	if err != nil {
		t.Errorf("Error creating app: %s", err)
	}

	viewTest := `<dashboard>
  <label>firstclassic</label>
  <description>firstclassic</description>
  <row>
    <panel>
      <table>
        <search>
          <query>| from inputlookup:"geo_attr_countries"</query>
          <earliest>-24h@h</earliest>
          <latest>now</latest>
        </search>
        <option name="drilldown">none</option>
      </table>
    </panel>
  </row>
</dashboard>`
	viewTest2 := `<dashboard>
  <label>firstclassic</label>
  <description>firstclassic</description>
  <row>
    <panel>
      <table>
        <search>
          <query>| from inputlookup:"geo_attr_countriess"</query>
          <earliest>-24h@h</earliest>
          <latest>now</latest>
        </search>
        <option name="drilldown">none</option>
      </table>
    </panel>
  </row>
</dashboard>`
	tmpFile, err := ioutil.TempFile(os.TempDir(), "prefix-")
	if err != nil {
		t.Error("Failed to create temporary file")
	}


	// Write view to file
	text := []byte(viewTest)
	if _, err = tmpFile.Write(text); err != nil {
		t.Error("Failed to write view to temp file")
	}

	// Close the file
	if err := tmpFile.Close(); err != nil {
		t.Error(err)
	}



	viper.Set("SPLUNK_URL", os.Getenv("SPLUNK_URL"))
	viper.Set("SPLUNK_USER", os.Getenv("SPLUNK_USER"))
	viper.Set("SPLUNK_PASSWORD", os.Getenv("SPLUNK_PASSWORD"))
	collectFlags()

	//delete everything for user.
	views, err := GetSplunkViewsForApp(appname, splunkUser, false)
	for _, v := range views {
		DeleteSplunkView(appname, v)
	}

	//create a new view
	created := CreateUpdateView(appname, tmpFile.Name())
	if !created {
		t.Errorf("Did not create view")
	}


	tmpFile, err = os.OpenFile(tmpFile.Name(), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	// Write view to file
	text = []byte(viewTest2)
	if _, err = tmpFile.Write(text); err != nil {
		t.Error("Failed to write view to temp file")
	}

	// Close the file
	if err := tmpFile.Close(); err != nil {
		t.Error(err)
	}

	//update a new view
	updated := CreateUpdateView(appname, tmpFile.Name())
	if !updated {
		t.Errorf("Did not update view")
	}

	//delete that view
	deleted := DeleteSplunkView(appname, "firstclassic")
	if !deleted {
		t.Errorf("Did not create view")
	}
	// Remember to clean up the file afterwards
	defer os.Remove(tmpFile.Name())
	err = DeleteSplunkApp(appname)
	if err != nil {
		t.Errorf("Error deleting app: %s", err)
	}
}